#include<iostream>
using namespace std;
class Box{
   float l,w,h;
public:
   void welcomeMessage();
   float boxArea(float l,float w ,float h)
   {     
           float a = 2*(l*w + l*h + w*h);
           return a;
   }
   float boxVolume(float l,float w ,float h);
   friend void displayBoxDimensions(Box);
   void setFun( float l,float w ,float h);
};
void Box::setFun(float l1,float w1 ,float h1) {
  l = l1;
  w = w1;
  h = h1;
}
void inline Box :: welcomeMessage()
   {
       cout<< "...Welcome to my program..."<<endl;
   }
void displayBoxDimensions(Box o)
{
   cout<<"Length is : " << o.l<< endl;
   cout<<"Width is : " << o.w<< endl;
   cout<<"Height is : " << o.h<< endl;
}
float Box :: boxVolume(float l,float w ,float h)
{
float v = l*w*h;
return v;
}
int main()
{
   Box obj;
   obj.welcomeMessage();
   float length,width,height;
   cout << "Enter the length,width,height of Box : ";
   cin >> length>> width >> height;
   obj.setFun(length,width,height);
   if (length>0 && width>0 && height > 0)
   {
   displayBoxDimensions(obj);
   cout << "The Area of the box is : " << obj.boxArea(length,width,height) << endl;
   cout << "The Volume of the box is : " << obj.boxVolume(length,width,height) << endl;
   }
   else
   {
       cout << "Invalid - Dimensions cannot be negative" << endl;
   }
}
