#include<iostream>
#include<iomanip>
using namespace std;
int main(){
    float a=2.2024345;
    cout<<"**USAGE OF ENDL Manipulator**"<<endl;
    cout<<"HELLO"<<endl;
    cout<<endl;
    cout<<"Hi nithin"<<endl;
    cout<<"**USAGE OF ENDS Manipulator**"<<endl;
    cout<<"Nithin"<<ends;
    cout<<"Kumar";
    cout<<endl;
    cout<<"**USAGE OF SETW Manipulator**"<<endl;
    cout<<"12345678"<<endl;
    cout<<setw(10)<<"hii"<<endl;
    cout<<"**USAGE OF SETFILL Manipulator**"<<endl;
    cout<<setfill('*')<<setw(10)<<"hii"<<endl; //by default it takes from riht to left
    cout<<setfill('*')<<left<<setw(10)<<"hii"<<endl;
    cout<<"**USAGE OF SETPRICISION Manipulator"<<endl;
    cout<<setprecision(4)<<a;
}

