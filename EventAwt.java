import java.awt.*;
import java.awt.event.*;
 
//Simple Registration Form in Java AWT
class MyApp extends Frame {
	Label lblTitle,lblName, lblContest,lblAge,lblGender,lblCourse;
	TextField txtName, txtContest,txtAge;
	Checkbox checkMale, checkFemale;
	CheckboxGroup cbg;
	Choice Course;
	Button btnSave,btnClear;
 
	public MyApp() {
		super("User Registration Form");
		setSize(1000, 600);// w,h
		setLayout(null);
		setVisible(true);
		setBackground(Color.DARK_GRAY);
 
 
		lblTitle=new Label("Registration Form for Coding Contest.");  
		lblTitle.setBounds(250,40,300,50);
		lblTitle.setForeground(Color.YELLOW);
		add(lblTitle);
 
		lblName=new Label("Name");  
		lblName.setBounds(250,100,150,30); 
		lblName.setForeground(Color.WHITE);
		add(lblName);
 
		txtName=new TextField();
		txtName.setBounds(400,100,400,30);
		add(txtName);
 
		 lblContest=new Label("Gmail");  
		 lblContest.setBounds(250,150,150,30);
		 lblContest.setForeground(Color.WHITE);
		add( lblContest);
 
		 txtContest=new TextField();
		 txtContest.setBounds(400,150,400,30);
		add( txtContest);
 
		lblAge=new Label("Age");  
		lblAge.setBounds(250,200,150,30);
		lblAge.setForeground(Color.WHITE);
		add(lblAge);
 
		txtAge=new TextField();
		txtAge.setBounds(400,200,400,30);
		add(txtAge);
 
 
		lblGender=new Label("Gender");  
		lblGender.setBounds(250,250,150,30);
		lblGender.setForeground(Color.WHITE);
		add(lblGender);
 
		cbg = new CheckboxGroup();
 
		checkMale = new Checkbox("Male",cbg,true);
		checkMale.setBounds(400,250, 100, 30);

		checkMale.setForeground(Color.WHITE);
		add(checkMale);
 
		checkFemale = new Checkbox("Female",cbg,false);
		checkFemale.setBounds(500,250, 100, 30);
		checkFemale.setForeground(Color.WHITE);
		add(checkFemale);
 
		lblCourse=new Label("Language Preference");  
		lblCourse.setBounds(250,300,150,30);
		lblCourse.setForeground(Color.WHITE);
		add(lblCourse);
 
		Course= new Choice();
		Course.setBounds(400, 300, 400, 50);
		Course.add("C");
		Course.add("C++");
		Course.add("Java");
		Course.add("C#");
		Course.add("Python");
		add(Course);
 

		btnSave=new Button("Save Details");
		btnSave.setBounds(400,530,150,30);

		btnSave.setBackground(Color.BLUE);
		btnSave.setForeground(Color.WHITE);
		add(btnSave);
 
		btnClear=new Button("Clear All");
		btnClear.setBounds(560,530,150,30);
		btnClear.setBackground(Color.RED);
		btnClear.setForeground(Color.WHITE);
		add(btnClear);
 
 
		// Close Button Code
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
	}
 
}
 
public class app {
	public static void main(String[] args) {
		MyApp frm = new MyApp();
	}
 
}




